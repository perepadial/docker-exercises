#### Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile).

**CMD** especifica un comando por defecto cuando se hace el run del contenedor sin ningún parámetro. En caso de especificar un comando al levantar el contenedor, el valor de CMD será sobreescrito. Esta instrucción normalmente se usa para levantar servicios de forma automatica al levantar el contenedor. 

**ENTRYPOINT** define el ejecutable de la instrucción de entrada al levantar el contenedor. Por ejemplo, se puede definir un entrypoint como python3 así los valores de entrada del comando para levantar el contenedor se ejecutarán con python versión 3. Se utiliza cuando quieres usar tu contenedor como un ejecutable. A diferencia de CMD, los comandos no pueden sobreescribirse. 
