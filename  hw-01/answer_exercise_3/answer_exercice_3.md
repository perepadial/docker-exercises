#### Pasos para la realización de la práctica

1. Ejecutamos una imagen docker nginx especificando el volument static_content:

docker run -it -v static_content:/usr/share/nginx/html nginx:1.19.3-alpine sh

2. realizamos un update del apk e instalamos vim

apk update
apk add vim

3. modificamos el fichero index.html con vim para mostrar lo que especifica el enunciado (fichero en la misma carpeta).

4. guardamos el fichero y salimos del contenedor con exit.

5. comprovamos que no hay ningún contenedor activo llamando con docker ps

6. comprovamos que el volumen se ha creado correctamente:

docker volume ls

7. creamos un nuevo contenedor en background haciendo el port mapping y especificando el volumen:

docker run -d -p 8080:80 -v static_content:/usr/share/nginx/html nginx:1.19.3-alpine

8. desde host hacemos un curl al puerto 8080:
   curl localhost:8080

<!DOCTYPE html>
<html>
<head>
<title>HOMEWORK 1</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>HOMEWORK 1</h1>
</body>
</html>
